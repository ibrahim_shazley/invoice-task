﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
   public class ProductDTO
    {
        public int id { get; set; }
        public string product_ar { get; set; }
        public string product_en { get; set; }
        public decimal price { get; set; }


    }
}
