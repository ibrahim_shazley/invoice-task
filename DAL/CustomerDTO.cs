﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class CustomerDTO
    {
        public int id { get; set; }
        public string name_ar { get; set; }
        public string name_en { get; set; }

    }
}
