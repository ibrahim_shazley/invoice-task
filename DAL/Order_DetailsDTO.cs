﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
   public class Order_DetailsDTO
    {
        public int id { get; set; }
        public int order_id { get; set; }
        public int product_id { get; set; }
        public decimal price { get; set; }
        public int quantity { get; set; }
        public decimal Total_price { get; set; }
        public List<Product> prod { get; set; }



    }
}
