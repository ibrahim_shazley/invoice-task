﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
   public class OrderDTO
    {
        public int OrderID { get; set; }
        public DateTime OrderDate { get; set; }
        public int ? Customer_ID { get; set; }
        public  decimal order_total { get; set; }
        public IEnumerable<Order_Details>Order_details { get; set; }
       // public List<Customer> customers { get; set; }
    }
}
