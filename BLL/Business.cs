﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using System.Data.SqlClient;

namespace BLL
{
    public class Business
    {
        invoice_dbEntities _Context;
        public Business()
        {
            _Context = new invoice_dbEntities();
        }

        public IEnumerable<CustomerDTO> GetAllCustomers()
        {
            return new invoice_dbEntities().Customer.SqlQuery("exec [dbo].[sp_GetCustomer]").ToList().Select(x => new CustomerDTO
            {
                id=x.ID,
                name_ar=x.name_ar,
                name_en=x.name_en

            });
        }
        public IEnumerable<ProductDTO> GetAllProducts()
        {
            return new invoice_dbEntities().Product.SqlQuery("exec [dbo].[sp_GetProducts]").ToList().Select(x => new ProductDTO
            {
                id = x.ID,
                product_ar = x.product_ar,
                product_en = x.product_en,
                price=(decimal)x.price

            });
        }

        public IEnumerable<OrderDTO> GetOreders()
        {
            return new invoice_dbEntities().Order.SqlQuery("exec [dbo].[sp_GetOrders]").ToList().Select(x => new OrderDTO
            {
                OrderID = x.ID,
                OrderDate = (DateTime)x.order_date,
                Customer_ID = x.customer_id,
                order_total = (decimal)x.order_total,
               

            });
        }
        public bool AddInvoice(int flag, Order order, List<Order_Details> order_details)
        {
            if(flag==1)
            {
                new invoice_dbEntities().CNT.SqlQuery($"exec [dbo].[sp_OrderDetails] {1},null,null,null,null,null,{order.customer_id},{order.order_total}").ToList();
             
                return true;
            }
            else if(flag==2)
            {
                foreach (var item in order_details)
                {
                    
                    new invoice_dbEntities().CNT.SqlQuery($"exec [dbo].[sp_OrderDetails] {2},null,{item.product_id},{item.price},{item.Quantity},{item.Total_price},null,null").ToList();
                   
                    return true;
                    
                }
            }
            return false;
           
        }

    }
}
