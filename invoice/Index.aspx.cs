﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using DAL;
using BLL;
using Newtonsoft.Json;


namespace invoice
{
    public partial class Index : System.Web.UI.Page
    {
        public static Business business = new Business();

        JsonSerializer serializer = new JsonSerializer();
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod()]
        public static string GetCustomers()
        {
            return JsonConvert.SerializeObject(business.GetAllCustomers());
        }
        [WebMethod()]
        public static string GetProducts()
        {
            return JsonConvert.SerializeObject(business.GetAllProducts());
        }
        [WebMethod()]
        public static string GetOrder()
        {
            return JsonConvert.SerializeObject(business.GetOreders());
        }

        //public static bool AddInvoice(int? order_id, int? product_id, decimal? price, int? quantity, decimal? total_price, int? customer_id, int? order_total)
        //{
        //    return business.AddInvoice(order_id,product_id,price,quantity,total_price,  customer_id, order_total);
        //}
    
        [WebMethod()]
        public static bool AddInvoice(int flag, Order order, List<Order_Details> order_details)
        {
            return business.AddInvoice( flag, order, order_details);
        }

    }
}