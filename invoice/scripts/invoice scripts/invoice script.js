﻿function GetAllCustomers() {
    jQuery.ajax({
        url: 'Index.aspx/GetCustomers',
        type: "POST",
        dataType: "json",
        data: "",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            let items = JSON.parse(data.d);
            console.log(items);
            var _select = '<option value="-1">اسم العميل</option>';
            for (var i = 0; i < items.length; i++) {
                let item = items[i];
                _select += '<option value="' + item.id + '">' + item.name_ar + '</option>';

                $('#dropcst').html(_select);

            }


        }
    });

}

var lastitem = false;
function GetOrder() {
    jQuery.ajax({
        url: 'Index.aspx/GetOrder',
        type: "POST",
        dataType: "json",
        data: "",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            let orderitem = JSON.parse(data.d);
            lastitem = (orderitem[orderitem.length - 1].OrderID) + 1;
            $('#invnumber').append('<span>' + lastitem + '</span>')

        }
    });

}
function quantitychange(element) {
    let row = $(element).closest('tr');
    var x = $(row).find("#txtquantity").val();
    total = (x * price);
    $(row).find("#total").children().val(total);
    totalchange();

}
var alltotal = 0;
function totalchange() {
    alltotal = alltotal + total;
    $('#alltotals').html("");
    $('#alltotals').append('<span>' + alltotal + '</span>');


}