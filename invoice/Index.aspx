﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="invoice.Index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    

    <script>
        function AppendRow() {
            let i = $('#tab_logic tbody tr').length;
            $('#tab_logic').append('<tr id="addr' + (i + 1) + '">\
                <td id="total" class="total"> <input type="text" value="" disabled="disabled" onchange="totalchange(this)"/> </td>\
                <td> <input type="number" id="txtquantity" onchange="quantitychange(this)" class="quantity"  name="quantity"/></td>\
                <td id="tdprice" class="tdprice"><span></span></td>\
                <td><span ><select class="product" id="dropprod" onchange="changedrop(this)" name="products"></select></span></td>\
                <td id="ser" style="width:5px";>' + (i + 1) + '</td> </tr>');
            GetAllProducts();
            //  GetAllCustomers();
        }

        $(document).ready(function () {
            GetOrder();
            GetAllProducts();
            GetAllCustomers();

            $('#addline').click(() => {
                AppendRow();
            });
        });
        </script>

    	<header>
			<h1>invoice</h1>
			
		</header>
		<article>
			<h1>Recipient</h1>
			
			<table class="meta" >
				<tr>
					
					<th id="invnumber"> </th>
                    <th><span >رقم الفاتورة </span></th>
				</tr>
				<tr>
					
					<th id="invdate"></th>
                    <th><span >تاريخ الفاتورة </span></th>
				</tr>
			
			</table>
           
                   <select class="form-control" id="dropcst"  name="customerDropdown"></select>
               	      <span >العميل </span>
                
			<table class="inventory"  id="tab_logic">
				<thead>
					<tr>
						<th><span >الإجمالى</span></th>
						<th><span >الكمية</span></th>
						<th><span >سعر الوحدة</span></th>
						<th><span >الأصناف</span></th>
                        <th><span >م</span></th>
					</tr>
				</thead>
				<tbody class="tbody">
					<tr id="addr1" >
						<td id="total" class="total" onchange="totalchange(this)"> <input type="text" value="" disabled="disabled" onchange="totalchange(this)"/> </td>
						<td> <input type="number" id="txtquantity" onchange="quantitychange(this)" class="quantity"  name="quantity"/></td>
						<td id="tdprice" class="tdprice"><span></span></td>
						<td><span ><select class="product" id="" onchange="changedrop(this)" name="products"></select></span></td>
                        <td id="ser" style="width:5px";>1</td>
					</tr>
				</tbody>
			</table> 
            <span id="btnid" ></span>
            <button id="addline" class="add" value="add line" onclick="return false">+</button>
			<table class="balance">
				<tr>
					<td id="alltotals"></td>
                    <th><span >الإجمالى </span></th>
				</tr>
			</table>

            <button type="button" id="addinvoice" onclick="AddInvoice()" style="background-color: #4CAF50;border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;"  value="add invoice" >Add Invoice</button>
		</article>

		<script>


		    var dt = new Date();
		    document.getElementById("invdate").innerHTML = dt.toLocaleDateString();


		    //function getprice() {
		    //var price = false;
		    //var total = false;
		    //    $('.product').change(function (e) {
		    //    var ind = this.value - 1;
		    //    price = (items[ind]).price;
		    //    $(this).parent().parent().siblings("td.tdprice").children('span').text(price);
		    //    total = (1 * price);
		    //    $("#total").text(total);


		    var price = 0;
		    var total = 0;
		    function changedrop(element) {
		        //  $(".product").on("change", function (e) {
		        let row = $(element).closest('tr');
		        let value = Number($(element).val());
		        let item = items.find(x => x.id === value);
		        price = (item).price;
		        $(row).find("#tdprice").text(price);
		        let total = (1 * price);
		        $(row).find("#total").children().val(total);
		        //  totalchange();

		    }


		    var items = [];
		    function GetAllProducts() {
		        jQuery.ajax({
		            url: 'Index.aspx/GetProducts',
		            type: "POST",
		            dataType: "json",
		            data: "",
		            contentType: "application/json; charset=utf-8",
		            success: function (data) {
		                items = JSON.parse(data.d);
		                let _select = $('#tab_logic tbody tr:last-child').find('select.product');
		                $(_select).append('<option value="-1">اسم الصنف </option>');
		                for (var i = 0; i < items.length; i++) {
		                    let item = items[i];
		                    $(_select).append('<option value="' + item.id + '">' + item.product_ar + '</option>');
		                }
		            }
		        });

		    }


		    //document.getElementById("addinvoice").addEventListener("click", function (event) {
		    //    event.preventDefault()
		    //});
		    var flag = false;
		    var order_details = [];
		    var product_id = false;
		    var Quantity = false;
		    let Total_price = total;
		    function AddInvoice() {
		        var customer_id = $('#dropcst').val();
		        var order_total = alltotal;
		        var order = { customer_id: customer_id, order_total: order_total };
		        var price = 0;
		        jQuery.ajax({
		            url: 'Index.aspx/AddInvoice',
		            type: "POST",
		            dataType: "json",
		            contentType: "application/json; charset=utf-8",
		            data: JSON.stringify({ flag: 1, order: order, order_details: null }),
		            success: function (data) {
		                alert("add to order table");
		              //  $('#tab_logic').each(function (i, tr) {
		                for (var i = 1; i < $('#tab_logic').find('tr').length; i++) {
		                    debugger;
		                    let row = $('#tab_logic').find('tr')[i];
		                    order_details = [];
		                    order_details.push({
		                        product_id: $(row).find($('.product option:selected')).val(),
		                        price: $(row).find('#tdprice').text(), Quantity: $(row).find("#txtquantity").val(),
		                        Total_price: $(row).find('#total').children().val()
		                    });

		                    alert(JSON.stringify(order_details));
		                   // order_details = [];
		                    jQuery.ajax({
		                        url: 'Index.aspx/AddInvoice',
		                        type: "POST",
		                        dataType: "json",
		                        contentType: "application/json; charset=utf-8",
		                        data: JSON.stringify({ flag: 2, order: null, order_details: order_details }),
		                        success: function (data) {
		                            alert("add to order details");
		                        }

		                    });
		                    alert("invoice added sucssefuly");
		                    window.location.href = '/Index.aspx';
		                }
                          
		            }
		                });
		                 
		                } 
                
		    
		    
		   
        





		    function addorderdetails() {
		        jQuery.ajax({
		            url: 'Index.aspx/AddInvoice',
		            type: "POST",
		            dataType: "json",
		            contentType: "application/json; charset=utf-8",
		            data: JSON.stringify({ flag: 2, order: null, order_details: order_details }),
		            success: function (data) {
		              //  addorderdetails();
		                alert("invoice added sucssefuly");
		                //  window.location.href = '/Index.aspx';

		            }

		        });

		        
		    }
		    
		   








          </script>
               
          
	


</asp:Content>
